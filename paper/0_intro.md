# Abstract

The abstract of the paper

# Specifications Table
Here is the completed specifications table.

\begin{table*}[t]
\centering
\begin{tabular}{|r|r|r|r|r|r|r|r|r|r|r|r|r|}
\hline
 & vars & n & mean & sd & median & trimmed & min & max & range & skew & kurtosis & se \\
\hline
Enjoyment & 1 & 71 & 2.79 & 1.57 & 3 & 2.74 & 1 & 5 & 4 & 0.10 & -1.60 & 0.19 \\
\hline
Motivation & 2 & 71 & 2.65 & 1.64 & 2 & 2.56 & 1 & 5 & 4 & 0.22 & -1.67 & 0.19 \\
\hline
Grade & 3 & 71 & 3.65 & 0.81 & 3 & 3.60 & 2 & 5 & 3 & 0.39 & -0.91 & 0.10 \\
\hline
\end{tabular}
\caption{\label{tbl:statistics}Descriptive statistics. \emph{sd} stands for \emph{standard deviation}
and \emph{se} for \emph{standard error}}
\end{table*}