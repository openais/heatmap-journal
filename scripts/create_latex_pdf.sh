#!/bin/sh
tlmgr install helvetic psnfss tex4ht times 
pandoc -s -F pandoc-crossref --natbib /paper/tex/meta.yaml --template=/paper/tex/mytemplate.tex -N -o /paper/tex/main.tex -f markdown -t latex+raw_tex /paper/*.md
cd /paper/tex
pdflatex /paper/tex/main.tex &> /dev/null
bibtex main &> /dev/null 
pdflatex /paper/tex/main.tex &> /dev/null
pdflatex /paper/tex/main.tex
 