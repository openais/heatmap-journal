# heatmap-journal

## Purpose
This is a repository to describe the data processing steps used to create the vessel density heatmap available at [VLIZ](www.somelink.com). There is an CI/CD pipeline attached to this project that builds up the text documents into a pdf file.